package com.facci.pm.delgado.anchundia.sigamvil.faccipmdelgadoanchundiasiga_movil;
import android.database.Cursor;

public class Profesor {
    private String facultad, materia, telefono, correo, nombre, apellido, imagen;
    private long id;
    public Profesor() {
    }
    public Profesor(String facultad, String materia, String telefono, String correo, String nombre, String apellido, String imagen) {
        this.facultad = facultad;
        this.materia = materia;
        this.telefono = telefono;
        this.correo = correo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.imagen = imagen;
    }
    public Profesor(String facultad, String materia, String telefono, String correo, String nombre, String apellido, String imagen, long id) {
        this.facultad = facultad;
        this.materia = materia;
        this.telefono = telefono;
        this.correo = correo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.imagen = imagen;
        this.id = id;
    }
    public String getFacultad() {
        return facultad;
    }
    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }
    public String getMateria() {
        return materia;
    }
    public void setMateria(String materia) {
        this.materia = materia;
    }
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getCorreo() {
        return correo;
    }
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getImagen() {
        return imagen;
    }
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    @Override
    public String toString() {
        return "Smartphone{" +
                "facultad='" + facultad + '\'' +
                ", materia=" + materia + '\'' +
                ", telefono=" + telefono + '\'' +
                ", correo" + correo + '\'' +
                ", nombre=" + nombre + '\'' +
                ", apellido=" + apellido + '\'' +
                ", imagen=" + imagen +
                '}';
    }
    public Profesor(Cursor cursor) {
        id = Long.parseLong(cursor.getString(cursor.getColumnIndex("id")));
        facultad = cursor.getString(cursor.getColumnIndex("facultad"));
        materia = cursor.getString(cursor.getColumnIndex("materia"));
        telefono = cursor.getString(cursor.getColumnIndex("telefono"));
        correo = cursor.getString(cursor.getColumnIndex("correo"));
        nombre = cursor.getString(cursor.getColumnIndex("nombre"));
        apellido = cursor.getString(cursor.getColumnIndex("apellido"));
        imagen = cursor.getString(cursor.getColumnIndex("imagen"));
    }
}