package com.facci.pm.delgado.anchundia.sigamvil.faccipmdelgadoanchundiasiga_movil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.squareup.picasso.Picasso;

import java.util.List;

 public class ResultListAdapter extends RecyclerView.Adapter<ResultListAdapter.MyViewHolder> {
    private List<Profesor> listaDeProfesores;
    public ResultListAdapter(List<Profesor> profesores) {
        this.listaDeProfesores = profesores;
    }
    public void setListaDeProfesores(List<Profesor> listaDeProfesores) {
        this.listaDeProfesores = listaDeProfesores;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View filaSmartphone = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_results_row, viewGroup, false);
        return new MyViewHolder(filaSmartphone);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Profesor profesor = listaDeProfesores.get(i);
          try {
            Picasso.get().load(profesor.getImagen()).into(myViewHolder.etImagen);
        }catch (Exception e){
           }
        myViewHolder.facultad.setText(profesor.getNombre());
        myViewHolder.materia.setText(profesor.getMateria());
    }
    @Override
    public int getItemCount() {
        return listaDeProfesores.size();
    }
    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView facultad, materia;
        ImageView etImagen;
        MyViewHolder(View itemView) {
            super(itemView);
            this.etImagen = itemView.findViewById(R.id.etImagen);
            this.facultad = itemView.findViewById(R.id.tvFacultad);
            this.materia = itemView.findViewById(R.id.tvMateria);
        }
    }
}
