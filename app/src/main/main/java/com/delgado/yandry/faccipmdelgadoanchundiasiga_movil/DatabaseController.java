package com.facci.pm.delgado.anchundia.sigamvil.faccipmdelgadoanchundiasiga_movil;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
public class DatabaseController {
    private BaseDeDatosHelper baseDeDatosHelper;
    private String TABLA_PROFESOR = "profesores";
    public DatabaseController(Context contexto) {
        baseDeDatosHelper = new BaseDeDatosHelper(contexto);
    }
    public int eliminarSmartphone(Profesor profesor) {
        SQLiteDatabase baseDeDatos = baseDeDatosHelper.getWritableDatabase();
        String[] argumentos = {String.valueOf(profesor.getId())};
        return baseDeDatos.delete(TABLA_PROFESOR, "id = ?", argumentos);
    }
    public int eliminarTodoSmartphone() {
        SQLiteDatabase baseDeDatos = baseDeDatosHelper.getWritableDatabase();
        return baseDeDatos.delete(TABLA_PROFESOR, "id = ?", null);
    }
    public ArrayList<Profesor> ConsultarProfesores(long id) {
        ArrayList<Profesor> profesores = new ArrayList<>();
        SQLiteDatabase baseDeDatos = baseDeDatosHelper.getReadableDatabase();
        String[] columnasAConsultar = {"facultad", "materia", "telefono", "correo", "nombre", "apellido", "imagen", "id"};
        //    String columns[] = new String[]{TABLA_PROFESOR};
        String selection = "id = ?"; // WHERE id LIKE ?
        String selectionArgs[] = new String[]{String.valueOf(id)};
        Cursor cursor = baseDeDatos.query(
                TABLA_PROFESOR,
                columnasAConsultar,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        if (cursor == null) {
            return profesores;
        }
        if (!cursor.moveToFirst()) return profesores;
        do {
            String facultadObtenidoDeBD = cursor.getString(0);
            String materiaObtenidaDeBD = cursor.getString(1);
            String telefonoObtenidoDeBD = cursor.getString(2);
            String correoObtenidaDeBD = cursor.getString(3);
            String nombreObtenidoDeBD = cursor.getString(4);
            String apellidoObtenidaDeBD = cursor.getString(5);
            String imagenObtenidoDeBD = cursor.getString(6);
            long idSmartphone = cursor.getLong(7);
            Profesor profesorObtenidoDeBD = new Profesor(facultadObtenidoDeBD, materiaObtenidaDeBD, telefonoObtenidoDeBD, correoObtenidaDeBD, nombreObtenidoDeBD, apellidoObtenidaDeBD, imagenObtenidoDeBD, idSmartphone);
            profesores.add(profesorObtenidoDeBD);
        } while (cursor.moveToNext());
        cursor.close();
        return profesores;
    }
    public Cursor consultarProfesor(String lawyerId) {
        SQLiteDatabase baseDeDatos = baseDeDatosHelper.getReadableDatabase();
        String campoParaActualizar = "id = ?";
        Cursor c = baseDeDatos.query(
                TABLA_PROFESOR,
                null,
                campoParaActualizar,
                new String[]{lawyerId},
                null,
                null,
                null);
        return c;
    }
    public long nuevoSmartphone(Profesor profesor) {
        SQLiteDatabase baseDeDatos = baseDeDatosHelper.getWritableDatabase();
        ContentValues valoresParaInsertar = new ContentValues();
        valoresParaInsertar.put("facultad", profesor.getFacultad());
        valoresParaInsertar.put("materia", profesor.getMateria());
        valoresParaInsertar.put("telefono", profesor.getTelefono());
        valoresParaInsertar.put("correo", profesor.getCorreo());
        valoresParaInsertar.put("nombre", profesor.getNombre());
        valoresParaInsertar.put("apellido", profesor.getApellido());
        valoresParaInsertar.put("imagen", profesor.getImagen());
        return baseDeDatos.insert(TABLA_PROFESOR, null, valoresParaInsertar);
    }
    public int guardarCambios(Profesor profesorEditado) {
        SQLiteDatabase baseDeDatos = baseDeDatosHelper.getWritableDatabase();
        ContentValues valoresParaActualizar = new ContentValues();
        valoresParaActualizar.put("facultad", profesorEditado.getFacultad());
        valoresParaActualizar.put("materia", profesorEditado.getMateria());
        valoresParaActualizar.put("telefono", profesorEditado.getTelefono());
        valoresParaActualizar.put("correo", profesorEditado.getCorreo());
        valoresParaActualizar.put("nombre", profesorEditado.getNombre());
        valoresParaActualizar.put("apellido", profesorEditado.getApellido());
        valoresParaActualizar.put("imagen", profesorEditado.getImagen());
        String campoParaActualizar = "id = ?";
        String[] argumentosParaActualizar = {String.valueOf(profesorEditado.getId())};
        return baseDeDatos.update(TABLA_PROFESOR, valoresParaActualizar, campoParaActualizar, argumentosParaActualizar);
    }
    public ArrayList<Profesor> obtenerProfesores() {
        ArrayList<Profesor> profesores = new ArrayList<>();
        SQLiteDatabase baseDeDatos = baseDeDatosHelper.getReadableDatabase();
        String[] columnasAConsultar = {"facultad", "materia", "telefono", "correo", "nombre", "apellido", "imagen", "id"};
        Cursor cursor = baseDeDatos.query(
                TABLA_PROFESOR,
                columnasAConsultar,
                null,
                null,
                null,
                null,
                null
        );
        if (cursor == null) {
            return profesores;
        }
        if (!cursor.moveToFirst()) return profesores;
        do {
            String facultadObtenidoDeBD = cursor.getString(0);
            String materiaObtenidaDeBD = cursor.getString(1);
            String telefonoObtenidoDeBD = cursor.getString(2);
            String correoObtenidaDeBD = cursor.getString(3);
            String nombreObtenidoDeBD = cursor.getString(4);
            String apellidoObtenidaDeBD = cursor.getString(5);
            String imagenObtenidoDeBD = cursor.getString(6);
            long idSmartphone = cursor.getLong(7);
            Profesor profesorObtenidoDeBD = new Profesor(facultadObtenidoDeBD, materiaObtenidaDeBD, telefonoObtenidoDeBD, correoObtenidaDeBD, nombreObtenidoDeBD, apellidoObtenidaDeBD, imagenObtenidoDeBD, idSmartphone);
            profesores.add(profesorObtenidoDeBD);
        } while (cursor.moveToNext());
        cursor.close();
        return profesores;
    }
}