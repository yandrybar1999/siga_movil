package com.facci.pm.delgado.anchundia.sigamvil.faccipmdelgadoanchundiasiga_movil;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.facci.pm.delgado.anchundia.sigamvil.faccipmdelgadoanchundiasiga_movil.R;
public class DetailActivity extends AppCompatActivity {
    private TextView etNombre, etApellido, etFacultad, etMateria, etTelefono, etCorreo;
    long idSmartphone;
    ImageView etImagen;
    private ArrayList<Profesor> smarthponelist;
    private DatabaseController profesoresController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
            return;
        }
        profesoresController = new DatabaseController(DetailActivity.this);
        idSmartphone = extras.getLong("idSmartphone");
        smarthponelist = profesoresController.ConsultarProfesores(idSmartphone);

        etNombre = findViewById(R.id.etNombre);
        etApellido = findViewById(R.id.etApellido);
        etImagen = findViewById(R.id.etImagen);
        etFacultad = findViewById(R.id.etFacultad);
        etMateria = findViewById(R.id.etMateria);
        etTelefono = findViewById(R.id.etTelefono);
        etCorreo = findViewById(R.id.etCorreo);
        loadLawyer();
    }
    private void loadLawyer() {
        new consultarProfesorTask().execute();
    }
    private void showLawyer(Profesor profesor) {
        etFacultad.setText(profesor.getFacultad());
        etMateria.setText(profesor.getMateria());
        etTelefono.setText(profesor.getTelefono());
        etCorreo.setText(profesor.getCorreo());
        etNombre.setText(profesor.getNombre());
        etApellido.setText(profesor.getApellido());
        try {
            Picasso.get().load(profesor.getImagen()).into(etImagen);
        } catch (Exception e) {
        }
    }
    private class consultarProfesorTask extends AsyncTask<Void, Void, Cursor> {
        @Override
        protected Cursor doInBackground(Void... voids) {
            return profesoresController.consultarProfesor(String.valueOf(idSmartphone));
        }
        @Override
        protected void onPostExecute(Cursor cursor) {
            if (cursor != null && cursor.moveToLast()) {
                showLawyer(new Profesor(cursor));
            } else {
            }
        }
    }
}
