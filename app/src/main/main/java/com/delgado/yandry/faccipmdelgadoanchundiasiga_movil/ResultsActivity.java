package com.facci.pm.delgado.anchundia.sigamvil.faccipmdelgadoanchundiasiga_movil;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import com.facci.pm.delgado.anchundia.sigamvil.faccipmdelgadoanchundiasiga_movil.R;
public class ResultsActivity extends AppCompatActivity {
    private List<Profesor> listaDeProfesores;
    private RecyclerView recyclerView;
    private ResultListAdapter resultListAdapter;
    private DatabaseController profesoresController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        profesoresController = new DatabaseController(ResultsActivity.this);
        recyclerView = findViewById(R.id.recyclerViewProfesores);
        listaDeProfesores = new ArrayList<>();
        resultListAdapter = new ResultListAdapter(listaDeProfesores);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(resultListAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Profesor profesorSeleccionada = listaDeProfesores.get(position);
                Intent intent = new Intent(ResultsActivity.this, DetailActivity.class);
                intent.putExtra("idSmartphone", profesorSeleccionada.getId());
                startActivity(intent);
            }
            @Override
            public void onLongClick(View view, int position) {
                final Profesor profesorParaEliminar = listaDeProfesores.get(position);
                AlertDialog dialog = new AlertDialog
                        .Builder(ResultsActivity.this)
                        .setPositiveButton("Sí, eliminar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                profesoresController.eliminarSmartphone(profesorParaEliminar);
                                refrescarListaDeProfesores();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setTitle("Confirmar")
                        .setMessage("¿Eliminar smarthpone " + profesorParaEliminar.getFacultad() + "?")
                        .create();
                dialog.show();
            }
        }));
        profesoresController.nuevoSmartphone(new Profesor("Ingenieria de Sistemas", "Fisica", "0991418024", "miguel.bermudez@uleam.edu.ec", "Miguel Ceferino", "Bermudez Lucas", "https://carreras.uleam.edu.ec/facci/wp-content/uploads/sites/26/2019/11/ESP_0155-4470x2980.jpg"));
        profesoresController.nuevoSmartphone(new Profesor("Ingenieria de Sistemas", "programación movil", "0994617762", "joffre.panchana@uleam.edu.ec", "Joffre Edgardo", "Panchana Flores", "https://carreras.uleam.edu.ec/facci/wp-content/uploads/sites/26/2019/11/ESP_0159-4470x2980.jpg"));
        profesoresController.nuevoSmartphone(new Profesor("Ingenieria de Sistemas", "Analisis y diseño de bas de datos", "0991418343", "josé.reyes@uleam.edu.ec", "José Jacinto", "Reyes Cárdenas", "https://carreras.uleam.edu.ec/facci/wp-content/uploads/sites/26/2019/11/Jacinto.jpg"));
        profesoresController.nuevoSmartphone(new Profesor("Ingenieria de Sistemas", "programacion aplicada a la web", "0999129833", "john.cevallos@uleam.edu.ec", "John Antonio", "Cevallos Macías", "https://carreras.uleam.edu.ec/facci/wp-content/uploads/sites/26/2019/11/ESP_0199.jpg"));
        profesoresController.nuevoSmartphone(new Profesor("Ingenieria de sistemas", "Estructura de datos", "098172637", "robert.moreira@uleam.edu.ec", "Robert Wilfrido", "Moreira Centeno", "https://carreras.uleam.edu.ec/facci/wp-content/uploads/sites/26/2019/11/robert.jpg"));
        profesoresController.nuevoSmartphone(new Profesor("Ingenieria de Sistemas", "Aplicacion de sistemas operativos", "099182736", "johnny.larrea@uleam.edu.ec", "Johnny Javier", "Larrea Plua", "https://carreras.uleam.edu.ec/facci/wp-content/uploads/sites/26/2019/08/IMG-20191112-WA0023-410x273.jpg"));
        profesoresController.nuevoSmartphone(new Profesor("Ingenieria de Sistemas", "Sistemas Digitales", "099218374", "elsa.vera@uleam.edu.ec", "Elsa Patricia", "Vera Burgos", "https://carreras.uleam.edu.ec/facci/wp-content/uploads/sites/26/2019/11/ESP_0197.jpg"));
    }
    @Override
    protected void onResume() {
        super.onResume();
        refrescarListaDeProfesores();
    }
    public void refrescarListaDeProfesores() {
        if (resultListAdapter == null)
            return;
        profesoresController.eliminarTodoSmartphone();
        listaDeProfesores = profesoresController.obtenerProfesores();
        resultListAdapter.setListaDeProfesores(listaDeProfesores);
        resultListAdapter.notifyDataSetChanged();
    }
}
